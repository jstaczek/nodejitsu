var express = require('express');
var cons = require('consolidate');
var swig  = require('swig');
var http = require('http');
var sys = require('sys');
var _request = require('request');
var zipstream = require('zipstream');
var url = require('url');
var conf = require('./conf');

// Request an mp3 download with this url:
//
// http://madrona.nodejitsu.com/dl1?id=33742877&key=s-pu7Nl&filename=SongName.mp3
//
// Take one of these:
//
// http://api.soundcloud.com/tracks/33742877.json?client_id=YOUR_CLIENT_ID&secret_token=s-pu7Nl
// 
// to get the streamable track id which is embedded in the waveform URL.
//
function resolve(id, key, cb) {
    var resolveURL = 'http://api.soundcloud.com/tracks/' + id + '.json?client_id=YOUR_CLIENT_ID&secret_token=' + key;
    console.log("URL to resolve is " + resolveURL);
    http.get(resolveURL, function(res) {

        var resolvedData  = '';

        console.log("Response from resolve is: " + res.statusCode);

        res.on('data', function (chunk) {
            resolvedData += chunk;
        });

        res.on('end',function(){
            var obj = JSON.parse(resolvedData);
            //console.log(sys.inspect(obj.waveform_url));
            var parts = url.parse(obj.waveform_url, true);
            var uid = parts.pathname.split('.').shift();
            uid = uid.substring(1, uid.length - 2);
		    var songURL = "http://media.soundcloud.com/stream/" + uid + "?secret_token=" + key;
            //console.log("songURL is " + songURL);
            cb(songURL);
        });

    }).on('error', function(e) {

        console.log("Got error: " + e.message);
        response.write("Got error: " + e.message);
        response.end();
    });
}

var app = express();
app.engine('.html', cons.swig);
app.set('view engin', 'html');

// NOTE: Swig requires some extra setup
// This helps it know where to look for includes and parent templates
swig.init({
    root: '.',
    allowErrors: true // allows errors to be thrown and caught by express instead of suppressed by Swig
});
app.set('views', '.');

// dl1 - download one mp3 file
// 
app.get('/dl1', function(request, response) {

    var url_parts = url.parse(request.url, true);
    var query = url_parts.query;
    //console.log(sys.inspect(query));
    resolve(query.id, query.key, function(songURL) {
	
	http.get(songURL, function(res) {

	    //console.log("Got response: " + res.statusCode);
	    //console.log("New location is: " + res.headers.location);

	    var x = _request(res.headers.location);
	    
	    // THIS WORKED x.pipe(response);

	    // Handle single unzipped mp3 for now. This code works fine for zipping one file,
	    // should work for multiple.
	    // 
	    /* var zip = zipstream.createZip({ level: 1 });
	     zip.addFile(x, { name: 'test.mp3' }, function() {
		zip.finalize(function(written) { console.log(written + ' total bytes written'); });
	    }); 

	    zip.pipe(response); */

	    response.setHeader('Content-disposition', 'attachment; filename=' + query.filename);
	    x.pipe(response);

	}).on('error', function(e) {
	    console.log("Got error: " + e.message);
	    response.end();
	});
    });
});


// cat - the collection
//
app.get('/cat', function(request, response) {
    response.render('template.html', { pagename: 'awesome people', authors: ['Paul', 'Jim', 'Jane'] });
});

app.listen(conf.port);
