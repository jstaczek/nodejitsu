var conf = {
  // the application environment
  // "production", "development", or "test
  env: process.env.NODE_ENV || "development",
 
  // the port to bind
  port: process.env.PORT || 8080,
 
  // database settings
  /* database: {
    host: process.env.DB_HOST || "localhost:8091"
  } */
};
 
module.exports = conf;
